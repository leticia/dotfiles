source ~/.vim/vimrc

autocmd FileType html set fileencoding=utf8
"Sets .ejs, .eco syntax highlighting
au BufNewFile,BufRead *.ejs,*.eco set filetype=html
"Stuff to ignore when tab completing
set wildignore=*.o,*.obj,*~,*.jpg,*.png,*.gif,*/tmp/*,*.swp,*.zip,*/log/*,
"Stuff to ignore when ctrl-p
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn|jhw-cache)$'
"Sets Ag as searcher
let g:ackprg = 'ag --nogroup --nocolor --column'

"No .swp backup files
set noswapfile
"Set terminal to 256 colors
set t_Co=256
colorscheme xoria256

"Mapping the toggle paste mode
nnoremap <leader>c :set invpaste paste?<CR>

"Clean search results highlight
nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>
noremap <F4> :set hlsearch! hlsearch?<CR>

"Mapping to move lines up and down
nnoremap <C-j> :m+<CR>==
nnoremap <C-k> :m-2<CR>==
inoremap <C-j> <Esc>:m+<CR>==gi
inoremap <C-k> <Esc>:m-2<CR>==gi
vnoremap <C-j> :m'>+<CR>gv=gv
vnoremap <C-k> :m-2<CR>gv=gv

"Mapping tab navigation
nnoremap <Tab> gt<CR>
nnoremap <Tab> gT<CR>

"Map Mass Buffers Quitting
nnoremap <C-b> :bufdo :q<CR>

"Enabling Zencoding
let g:user_zen_settings = {
 \  'liquid' : {
 \     'extends' : 'html',
 \  },
 \  'ejs' : {
 \     'extends' : 'html',
 \  },
 \  'eco' : {
 \     'extends' : 'html'
 \  }
\}

let g:user_zen_expandabbr_key = '<C-e>'
let g:use_zen_complete_tag = 1
