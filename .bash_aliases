#============================================================#
#                                                            #
#  ALIASES                                                   #
#                                                            #
#============================================================#

alias mkdir='mkdir -p'
alias j='jobs -l'
alias which='type -a'
alias ..='cd ..'

# Pretty-print of some PATH variables:
alias path='echo -e ${PATH//:/\\n}'

alias du='du -kh' # Makes a more readable output.
alias df='df -kTh'

# Add colors for filetype and human-readable sizes by default on 'ls':
alias ls='ls -h --color'
# Directories first, with alphanumeric sorting:
alias ll="ls -lv --group-directories-first"

alias l='ls'
alias lr='ll -R' #  Recursive ls.
alias la='ll -A' #  Show hidden files.

# MPG123
alias all='mpg123 -zCv ~/music/**' # Play all songs
alias band=folder # Play folder
alias song=one # Play file

# Search for folders/songs
folder() {
  arg=$*;
  mpg123 -zCv ~/music/*"${arg}"*/**;
}

one() {
  arg=$*;
  mpg123 -zCv ~/music/**/*"${arg}"*.mp3;
}

# Git
alias pull='git pull origin $(__git_ps1 "%s")'
alias purr='git pull --rebase origin $(__git_ps1 "%s")'
alias push='git push origin $(__git_ps1 "%s")'
alias stash='git stash'
alias apply='git apply'
