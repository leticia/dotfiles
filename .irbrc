require 'irb/completion'
require 'irb/ext/save-history'
require 'tempfile'

IRB.conf[:SAVE_HISTORY] = 1000
IRB.conf[:HISTORY_FILE] = "#{ENV['HOME']}/.irb-save-history"
puts "History configured."

IRB.conf[:AUTO_INDENT]=true
puts "Auto-indent on."

begin
  require "rubygems"
  puts "Rubygems loaded."
rescue LoadError => e
  puts "Seems you don't have Rubygems installed: #{e}"
end
